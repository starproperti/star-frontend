import React, { Component } from 'react';
import {
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';
import Style from './login.css'

import Login from './component-login'

import MenuNavbar from './../Menu'
import Footer from './../Footer'
export default class login extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
				<Login/>
				<Footer/>
			</div>
			);
	}
}