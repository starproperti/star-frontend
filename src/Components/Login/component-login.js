import React, { Component } from 'react';
import {
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';
const styles = {
	transition: 'all .2s ease-out'
};

export default class ComponentLogin extends Component{
	constructor(props){
		super(props);
		this.state = {
			class:'',
			username:'',
			password:''
		};
		this.login = this.login.bind(this),
		this.onChange = this.onChange.bind(this)
	}
	onClass(){
        this.setState({class:this.state.class === 'label-push-login' ? 'label-push-login' : 'label-push-login'})
	}
	login(){
		console.log("login");
	}
	onChange(e){
		this.setState({[e.target.name]: e.target.value});
		console.log(this.state);
	}
	render(){
		return(
			<div className="container-top-sect">
				<div className="lg-pd" style={{background:"#f2f2f2"}}>
					<div className="container-box">
						<div className="title">
							Login
						</div>
						<Row>
							<Col sm="12">
								<div className="form-group-login">
									<label style={{...styles}} className={this.state.class}>
										Email
									</label>
									<input type="email" className="form-controls-login" name="username" onClick={this.onClass.bind(this)} onChange={this.onChange}/>
								</div>
							</Col>
							<Col sm="12">
								<div className="form-group-login">
									<label style={{...styles}} className={this.state.class}>
										Password
									</label>
									<input type="password" className="form-controls-login" name="password" onClick={this.onClass.bind(this)} onChange={this.onChange}/>
								</div>
							</Col>
							<Col>
								<div className="form-group-login">
									<input type="submit" className="form-controls-login" value="login" onClick={this.login}/>
								</div>
							</Col>
						</Row>
					</div>
				</div>
			</div>
			);
	}
}