import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { 
		 Container,
		 Row, 
		 Col
		} from 'reactstrap';
import style from './footer.css'

export default class Home extends Component{
	render(){
		return(
			<div className="container-footer">
				<div className="footerTop">
					<Container>
						<Row>
							<Col xs="12" sm="12" md="6" lg="3" xl="3">
								<h6>Tentang Star Properti</h6>
								Star Properti - Star properti adalah agen preperti terpercaya.Banyak konsumen kita yang sangat puas bekerja sama dengan kita Star ....
								<p style={{marginTop:10}}><Link to="/Tentang" style={{color:"#f2f2f2"}}>Baca Selengkapnya</Link></p>
							</Col>
							<Col xs="12" sm="12" md="6" lg="3" xl="3">
								<h6>Properti Terbaru</h6>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet ex nibh. Cras non quam aliquam, lobortis massa id, fringilla felis
							</Col>
							<Col xs="12" sm="12" md="6" lg="3" xl="3">
								<h6>Kontak Info</h6>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sit amet ex nibh. Cras non quam aliquam, lobortis massa id, fringilla felis
							</Col>
							<Col xs="12" sm="12" md="6" lg="3" xl="3">
								<h6>Link Menu</h6>
									<ul>
		                            <li>
		                                <Link to="/" style={{marginTop:68}}>Beranda</Link>
		                            </li>
		                            <li>
		                                <Link to="/Property">Property</Link>
		                            </li>
		                            <li>
		                                <Link to="/Portofolio">Portofolio</Link>
		                            </li>
		                            <li>
		                                <Link to="/Agent">Agents</Link>
		                            </li>
		                            <li>
		                                <Link to="/Tentang">Tentang</Link>
		                            </li>
		                            <li>
		                                <Link to="/Contact">Contact</Link>
		                            </li>
		                            <li className="login-dp">
		                                <Link to="/Register">Register</Link>
		                            </li>
		                            <li className="login-dp">
		                                <Link to="#">Sign In</Link>
		                            </li>
		                        </ul>
							</Col>
						</Row>
					</Container>
				</div>
			</div>
			);
	}
}