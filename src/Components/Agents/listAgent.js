import React, { Component } from 'react';
import {
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';
import {Link} from "react-router-dom";
import Style from './Property.css'

export default class listAgent extends Component{
	constructor(props){
		super(props);
		this.state = {
			agent: [],
		}
	}
	fetchData(){
		fetch('http://localhost/star-backend/StarPropertyRequest/api/agen/readAllAgen.php')
		.then(response => response.json())
		.then(parsedJSON => parsedJSON.data.map(parsprop => (
				{
					id:  `${parsprop.id}`,
					nama:  `${parsprop.nama}`,
					jenisKelamin:  `${parsprop.jenisKelamin}`,
					alamat:  `${parsprop.alamat}`,
					noTelepon:  `${parsprop.noTelepon}`,
					noKtp:  `${parsprop.noKtp}`,
					email:  `${parsprop.email}`,
					foto:  `${parsprop.foto}`,
					idLevel:  `${parsprop.idLevel}`,
				}
			)))
		.then(agent => this.setState({
			agent,
			isLoading: false
		}))
		.catch(error => /*alert('Kesalahan', error)*/ console.log('Kesalahan', error))
	}
	componentDidMount(){
		this.fetchData();
	}
	render(){
		const {agent} = this.state;
		return(
				<Container>
					<Row>
					{
						agent.length > 0 ? agent.map(prop => {
							const url ="http://localhost/star-backend/StarPropertyBack/tempUpload/properti/"+prop.foto;
						return <Col xl="4" lg="4" md="6" sm="12" sm="12" style={{marginTop:30}} key={prop.id}>
							<div className="box-agent">
								<div className="cards">
								  <img src={url} alt="John" style={{width:"45%"}}/>
								  <h3>{prop.nama}</h3>
								  <p className="title">Agent</p>
								  <p>{prop.noTelepon}</p>
								  <p><button>Contact</button></p>
								</div>
							</div>
						</Col>
						}) : null
					}
					</Row>
				</Container>
			);
	}
}