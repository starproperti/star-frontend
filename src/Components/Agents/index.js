import React, { Component } from 'react';
import {Link} from "react-router-dom";

import MenuNavbar from './../Menu'
import Footer from './../Footer'

import SectTop from './sect-top'
import Agent from './listAgent'

export default class Agents extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
				<SectTop/>

					<div className="our-property">
						<h3>Lorem Ipsum</h3>
						<p>Lorem Ipsum</p>
					</div>
					<div className="lg-pd">
						<Agent/>
					</div>

				<Footer/>
			</div>
			);
	}
}