import React, { Component } from 'react';
import Tentangs from './Tentang-Component'
import Menu from './../Menu'
import Footer from './../Footer'

export default class Tentang extends Component{
	render(){
		return(
			<div>
				<Menu/>
				<Tentangs/>
				<Footer/>
			</div>
			);
	}
}