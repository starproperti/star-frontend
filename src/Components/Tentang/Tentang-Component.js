import React, { Component } from 'react';
import {Link} from "react-router-dom";
import ContentPortofolioLimit from './../Portofolio/portofolioLimit'
import {
		 Row, 
		 Col
		} from 'reactstrap';

export default class TentangComponent extends Component{
	render(){
		return(
			<div className="container-top-sect lg-pd">
				<Row className="container-top-sect">
					<Col xs="12" sm="12" md="3" className="np">
						<h1>Tentang Kami</h1>
					</Col>
					<Col xs="12" sm="12" md="9">
						<div className="box-card">
							<h5>Tentang Kami</h5>
							<p>
								<b>Star Properti - </b> Star properti adalah sebuah agen bisnis dalam bidang properti seperti rumah/kost-kosan khususnya daerah malang. Salah satu kelebihan menjual properti dengan mudah dan terpercaya. Kost Exclusive yang nyaman dan berkualitas. Tunggu apa lagi? Survey lokasi dan booking segera!
							</p>
						</div>
					</Col>
				</Row>
				<div className="our-property">
					<h3>Portofolio</h3>
					<p></p>
				</div>
				<div>
					<ContentPortofolioLimit/>
					<div className="our-property">
						<Link to="Portofolio">Semua Portofolio</Link>
					</div>
				</div>
			</div>
			);
	}
}