import React, { Component } from 'react';
import {Link} from "react-router-dom";

import MenuNavbar from './../../Menu'
import Footer from './../../Footer'

import DetailPort from './component-detail-port'

import './detail-port.css'
export default class DetailPortofolio extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
				<DetailPort/>
				<Footer/>
			</div>
			);
	}
}