import React, { Component } from 'react';
import {Link} from "react-router-dom";

import MenuNavbar from './../Menu'
import Footer from './../Footer'

import SectTop from './sect-top'
import Content from './portofolio'

export default class Portofolio extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
				<SectTop/>

					<div className="our-property">
						<h3>Produk yang telah laku terjual</h3>
						<p></p>
					</div>
					<div className="lg-pd">
						<Content/>
					</div>

				<Footer/>
			</div>
			);
	}
}