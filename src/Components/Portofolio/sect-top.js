import React, { Component } from 'react';

export default class SectTopProp extends Component{
	render(){
		return(
			<div className="container-top-sect-properti">
				<div className="top-sect-properti">
					<div className="top-sect-layer-properti">
						<h2>Portofolio Kami</h2>
					</div>
                </div>
            </div>
			);
	}
}