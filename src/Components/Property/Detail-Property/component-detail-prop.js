import React, { Component } from 'react';
import {Link} from "react-router-dom";
import {
		 Card,
		 Container,
		 Row, 
		 Col
		} from 'reactstrap';
import rumah from './../../../assets/rumah.jpg'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
//import ImageGallery from 'react-image-gallery';
//import Lightbox from 'react-images';
import { Carousel } from 'react-responsive-carousel';

export default class ComponentDetailProperty extends Component{
	constructor(props){
		super(props);
		this.state= {
			properti: [],
			propertiImage: [],
			infiniteLoop:true
		}
	}

	fetch(){
		var url = window.location.pathname;
		var split = url.split("/").slice(-1)[0];
		fetch('http://localhost/star-backend/StarPropertyRequest/api/properti/readSingleProperti.php?id='+split)
		.then(response => response.json())
		.then(parsedJSON => parsedJSON.data.map(parsprop => (
			{
				id: `${parsprop.id}`,
				idUser: `${parsprop.idUser}`,
				idJenisProperti:  `${parsprop.idJenisProperti}`,
				namaProperti: `${parsprop.namaProperti}`,
				alamatProperti:  `${parsprop.alamatProperti}`,
				luasProperti:  `${parsprop.luasProperti}`,
				jumlahKamar:  `${parsprop.jumlahKamar}`,
				idKamarMandi:  `${parsprop.idKamarMandi}`,
				kamarMandiDalam : `${parsprop.jumlahKamarMandiDalam}`,
				kamarMandiLuar : `${parsprop.jumlahKamarMandiLuar}`,
				garasi : `${parsprop.garasi}`,
				deskripsiProperti:  `${parsprop.deskripsiProperti}`,
				hargaProperti:  `${parsprop.hargaProperti}`,
				idPenjualan:  `${parsprop.idPenjualan}`,
				totalLihat:  `${parsprop.totalLihat}`,
				idStatusProperti:  `${parsprop.idStatusProperti}`,
				idStatusPersetujuanProperti:  `${parsprop.idStatusPersetujuanProperti}`,
				addedBy:  `${parsprop.ditambahOleh}`,
				nama: `${parsprop.namaUser}`,
				jenisKelamin: `${parsprop.jenisKelamin}`,
				alamatUser: `${parsprop.alamat}`,
				noTelepon: `${parsprop.noTelepon}`,
				email: `${parsprop.email}`,
				fotoUser: `${parsprop.foto}`
			}
		)))
	.then(properti=>this.setState({
		properti
	}
		)).catch(error => console.log('Kesalahan', error));
	}

	fetchImage(){
		var urlImage = window.location.pathname;
		var splitImage = urlImage.split("/").slice(-1)[0];
		fetch('http://localhost/star-backend/StarPropertyRequest/api/properti/readAllImage.php?id='+splitImage)
		.then(response => response.json())
		.then(parsedJSON => parsedJSON.data.map(parsimg => (
			{
				id: `${parsimg.id}`,
				foto: `${parsimg.foto}`
			}
		)))
		.then(propertiImage=>this.setState({
			propertiImage
		}
		)).catch(error => console.log('Kesalahan', error));
	}

	componentDidMount(){
		this.fetch();
		this.fetchImage();
	}
	render(){
		const {properti} = this.state;
		const {propertiImage} = this.state;
		return(
			<div className="container-top-sect">
				<div className="lg-pd" style={{background:"#f7f8fa"}}>
				{
					properti.length > 0 ? properti.map(prop => {
						const detail = prop.deskripsiProperti;
						return <div key={prop.id}>
							<i className="fa fa-home" style={{color:"#129e94"}}></i> / Detail Produk / {prop.namaProperti}
							<div className="our-property" style={{textAlign:"left"}}>
								<h3>{prop.namaProperti}</h3>
								<small>{prop.alamatProperti}</small>
					        	<div className="label-type">
						          <div className={prop.idPenjualan == '1' ? 'label-type-sell' : 'label-type-rent'}>
						          	<span>{prop.idPenjualan == '1' ? "Dijual" : "Disewakan"}</span>
						          </div>
			 					  <div className="label-type-HotOffer" style={{ visibility: prop.totalLihat >= 100 ? 'visible' : 'hidden'}}> Hot Offer </div>
					          	</div>
							</div>
							<div>
							<Container className="np">
							<Row>
								<Col xs="12" xs="12" md="9">
								<Row> 
									<Col xs="12" xs="12" md="12">
										<Carousel style={{width:"100%"}} autoPlay infiniteLoop>
								{
									propertiImage.length > 0 ?propertiImage.map(propImg => {
										const url = 'http://localhost/star-backend/StarPropertyBack/tempUpload/properti/'+propImg.foto;
										console.log(url);
										return <div key={propImg.id}>
													<img src={url} style={{width:"100%"}}/>
												</div>
									}):null
								}
										</Carousel>
									</Col>
									<Col xs="12" lg="12">
										<div className="container-top-sect">
											<div className="box-card">
												<h5>Fasilitas : </h5>
												<Container>
													<Row>
														<Col xs="12" lg="3">
															<div className="dp-block-fasilitas">
																<i className="fa fa-home"></i> <b>Luas Tanah :</b> {prop.luasProperti}m<sup>2</sup>
															</div>
															<div className="dp-block-fasilitas">
																<i className="fa fa-bed"></i> <b>Jumlah Kamar :</b> {prop.jumlahKamar}
															</div>
														</Col>
														<Col xs="12" lg="3">
															<div className="dp-block-fasilitas">
																<i className="fa fa-bath"></i> <b>Kamar Mandi :</b> {prop.idKamarMandi == '1' ? "Dalam" : prop.idKamarMandi == '2' ? "Luar" : prop.idKamarMandi == '3' ? "Luar&Dlm" : "Belum Ada"}
															</div>
															<div className="dp-block-fasilitas">
																<i className="fa fa-bath"></i> <b>Kamar Mandi Dalam :</b> {prop.kamarMandiDalam}
															</div>
														</Col>
														<Col xs="12" lg="3">
															<div className="dp-block-fasilitas">
																<i className="fa fa-bath"></i> <b>Kamar Mandi Luar :</b> {prop.kamarMandiLuar}
															</div>
															<div className="dp-block-fasilitas">
																<i className="fa fa-car"></i> <b>Garasi :</b> {prop.garasi}
															</div>
														</Col>
														<Col xs="12" lg="3">
															<div className="dp-block-fasilitas">
																<i className="fa fa-bath"></i> <b>Tipe Rumah :</b> {prop.idJenisProperti == '1' ? "Kost" : "Rumah"}
															</div>
															<div className="dp-block-fasilitas">
																<i className="fa fa-bed"></i> <b>Penjualan :</b> {prop.idPenjualan == '1' ? "Dijual" : "Disewakan"}
															</div>
														</Col>
													</Row>
												</Container>
											</div>
										</div>
									</Col>
									<Col xs="12" lg="12">
										<div className="container-top-sect">
											<div className="box-card">
												<h5>Deskripsi : </h5>
												<span> {ReactHtmlParser(detail)} </span>
											</div>
										</div>
									</Col>
								</Row>
								</Col>
								<Col xs="12" xs="12" md="3" className="np">
									<div className="box-agent" style={{position:'sticky' , top:100}}>
										<div className="cards">
										  <img src={"http://localhost/star-backend/StarPropertyBack/tempUpload/"+prop.fotoUser} alt="John" style={{width:"45%"}}/>
										  <h3>{prop.nama}</h3>
										  <p className="title">Agent</p>
										  <p>{prop.alamatUser}</p>
										  <p><button>Contact</button></p>
										</div>
									</div>
								</Col>
							</Row>
							</Container>
							</div>
						</div>
					}) : null
				}
				</div>
			</div>
			);
	}
}