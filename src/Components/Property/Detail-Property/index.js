import React, { Component } from 'react';
import {Link} from "react-router-dom";

import MenuNavbar from './../../Menu'
import Footer from './../../Footer'

import DetailProp from './component-detail-prop'

import './detail-property.css'
export default class DetailProperty extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
				<DetailProp/>
				<Footer/>
			</div>
			);
	}
}