import React, { Component } from 'react';
import {Link} from "react-router-dom";

import MenuNavbar from './../Menu'
import Footer from './../Footer'

import SectTop from './sect-top'
import Content from './listProperty'

class Property extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
				<SectTop/>

					<div className="our-property">
						<h3>Kami Menyediakan Banyak Properti Sesuai Kebutuhan Anda</h3>
						<p></p>
					</div>
					<div className="lg-pd">
						<Content/>
					</div>

				<Footer/>
			</div>
			);
	}
}
export default Property;