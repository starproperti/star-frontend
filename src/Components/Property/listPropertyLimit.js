import React, { Component } from 'react';
import { Card,
		 CardImg, 
		 CardText, 
		 CardBody, 
		 CardTitle,
		 CardFooter,
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';
import {Link} from "react-router-dom";
import Style from './Property.css'

export default class listProperty extends Component{
	constructor(props){
		super(props);
		this.state = {
			properti: [],
		}
	}
	fetchData(){
		fetch('http://localhost/star-backend/StarPropertyRequest/api/properti/readNotSoldLimit.php')
		.then(response => response.json())
		.then(parsedJSON => parsedJSON.data.map(parsprop => (
				{
					id: `${parsprop.id}`,
					idUser: `${parsprop.idUser}`,
					idJenisProperti:  `${parsprop.idJenisProperti}`,
					namaProperti: `${parsprop.namaProperti}`,
					alamatProperti:  `${parsprop.alamatProperti}`,
					luasProperti:  `${parsprop.luasProperti}`,
					jumlahKamar:  `${parsprop.jumlahKamar}`,
					idKamarMandi:  `${parsprop.idKamarMandi}`,
					jumlahKamarMandiDalam : `${parsprop.jumlahKamarMandiDalam}`,
					jumlahKamarMandiLuar : `${parsprop.jumlahKamarMandiLuar}`,
					totalKamarMandi : `${parsprop.jumlahKamarMandiDalam}` + `${parsprop.jumlahKamarMandiLuar}`,
					garasi: `${parsprop.garasi}`,
					deskripsiProperti:  `${parsprop.deskripsiProperti}`,
					hargaProperti:  `${parsprop.hargaProperti}`,
					idPenjualan:  `${parsprop.idPenjualan}`,
					totalLihat:  `${parsprop.totalLihat}`,
					idStatusProperti:  `${parsprop.idStatusProperti}`,
					idStatusPersetujuanProperti:  `${parsprop.idStatusPersetujuanProperti}`,
					thumbnail:`${parsprop.thumbnail}`,
					addedBy:  `${parsprop.ditambahOleh}`
				}
			)))
		.then(properti => this.setState({
			properti,
			isLoading: false
		}))
		.catch(error => /*alert('Kesalahan', error)*/ console.log('Kesalahan', error))
	}
	componentDidMount(){
		this.fetchData();
	}

	render(){
		const {properti} = this.state;
		return(
				<Container>
					<Row>
					{
						properti.length > 0 ? properti.map(prop => {
						const url ="http://localhost/star-backend/StarPropertyBack/tempUpload/properti/"+prop.thumbnail;
						return <Col key={prop.id} xl="4" lg="4" md="6" sm="12" sm="12" style={{marginTop:30}}>
					      <Card>
					      	<Link to={{pathname:'/Detail-Property/Property/'+prop.id}}>
					      	<div className="container-image-card">
				        			<CardImg top width="100%" src={url} alt="Card image cap" />
					        	<div className="label-price">
					        		<span>Rp. {prop.hargaProperti}</span>
					        	</div>
					        </div>
					        <CardBody>
					        	<div className="label-type">
						          <div className={prop.idPenjualan == '1' ? 'label-type-sell' : 'label-type-rent'}>
						          	<span>{prop.idPenjualan == '1' ? "Dijual" : "Disewakan"}</span>
						          </div>
						          <div className="label-type-HotOffer" style={{ visibility: prop.totalLihat >= 100 ? 'visible' : 'hidden'}}> Hot Offer </div>
					          	</div>
					          <CardTitle>{prop.namaProperti}</CardTitle>
					          <CardText>{prop.alamatProperti}</CardText>
					        </CardBody>
        					<CardFooter style={{textAlign:"center"}}>
        						<span style={{marginRight:10}} title="Luas Properti"><i className="fa fa-home"></i> {prop.luasProperti} m<sup>2</sup></span>
        						<span style={{marginRight:10}} title="Kamar Mandi"><i className="fa fa-bath"></i> {prop.totalKamarMandi} </span>
        						<span style={{marginRight:10}} title="Kamar Tidur"><i className="fa fa-bed"></i> {prop.jumlahKamar} </span>
        						<span style={{marginRight:10}} title="Total Lihat"><i className="fa fa-eye"></i> {prop.totalLihat}x </span>
        					</CardFooter>
        					</Link>
					      </Card>

						</Col>
						}) : null
					}
					</Row>
				</Container>
			);
	}
}