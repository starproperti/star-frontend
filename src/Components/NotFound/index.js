import React, { Component } from 'react';
import {
		 Button,
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';
import {Link} from "react-router-dom";
import notFound from './../../assets/404-1.png'
import './notfound.css'

import MenuNavbar from './../Menu'
import Footer from './../Footer'

export default class NotFound extends Component{
	render(){
		return(
			<div style={{backgroundColor:"#16A085"}}>
				<MenuNavbar/>
					<div className="container-notfound">
						<div className="notfound-inside">
							<Container>
								<Row>
									<Col md="6" lg="6" style={{textAlign:'right'}}>
										<img src={notFound} style={{width:"90%"}}/>
									</Col>
									<Col md="6" lg="6" className="notfound-404">
										<h1>404</h1>
										<p>Kamu Nyasar</p>
										<p>Gak ada apa apa disini :(</p>
										<Link to="/"><Button className="uppercase-text">Pulang Kerumah</Button></Link>
									</Col>
								</Row>
							</Container>
						</div>
					</div>
				<Footer/>
			</div>
			);
	}
}