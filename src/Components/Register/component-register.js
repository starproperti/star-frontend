import React, { Component } from 'react';
import {
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';

import {Redirect} from 'react-router-dom';
import {PostUserData} from '../../Services/UsersAPI.js';
import axios from 'axios';

const styles = {
	transition: 'all .2s ease-out'
};
export default class ComponentRegister extends Component{
	constructor(props){
		super(props);
		this.state = {
			class:'',

			pertanyaan: [],
			nama : '',
			email: '',
			username: '',
			password: '',
			idPertanyaanKeamanan: '1',
			jawabanKeamanan: '',
			redirectToNewPage : false
		};
		this.handleChange = this.handleChange.bind(this);
		this.handelSubmit = this.handelSubmit.bind(this);
	}
	handleChange = event => {
		this.setState({[event.target.name]:event.target.value});
	}
	handelSubmit = event => {
		event.preventDefault();
		const fd = new FormData();
		fd.append('nama', this.state.nama);
		fd.append('email', this.state.email);
		fd.append('username', this.state.username);
		fd.append('password', this.state.password);
		fd.append('idPertanyaanKeamanan', this.state.idPertanyaanKeamanan);
		fd.append('jawabanKeamanan', this.state.jawabanKeamanan);

		axios.post(`http://localhost/star-backend/StarPropertyRequest/api/akun/tambahAkun.php`, fd, {
			headers:{
				'Content-Type':'multipart/form-data',
			}
		})
		.then(res => {
			console.log(res);
			console.log(res.data);
		})
	}
	componentDidMount(){
		this.fetchData();
	}
	fetchData(){
		fetch('http://localhost/star-backend/StarPropertyRequest/api/pertanyaan/readPertanyaan.php')
		.then(response => response.json())
		.then(parsedJSON => parsedJSON.data.map(parspertanyaan => (
				{
					id: `${parspertanyaan.id}`,
					pertanyaan: `${parspertanyaan.pertanyaan}`
				}
			)))
		.then(pertanyaan => this.setState({
			pertanyaan
		}))
		.catch(error => /*alert('Kesalahan', error)*/ console.log('Kesalahan', error));
	}
	// onClass(){
 //        this.setState({class:this.state.class === 'label-push-register' ? 'label-push-register' : 'label-push-register'})
	// }
	render(){
		const {pertanyaan} = this.state;
		if(sessionStorage.getItem('userData')){
			return(
					<redirect to="/" />
				)
		}
		return(
			<div className="container-top-sect">
				<div className="lg-pd" style={{background:"#f2f2f2"}}>
					<div className="container-box">
						<div className="title">
							daftar
							<br/>
						</div>
						<Row>
							<Col sm="12">
								<div className="form-group-register">
									<label style={{...styles}} className={this.state.class}>
										Nama
									</label>
									<input type="text" name="nama" className="form-controls-register" onChange={this.handleChange} />
								</div>
							</Col>
							<Col sm="12">
								<div className="form-group-register">
									<label style={{...styles}} className={this.state.class}>
										Email
									</label>
									<input type="email" name="email" className="form-controls-register" onChange={this.handleChange} />
								</div>
							</Col>
							<Col sm="12">
								<div className="form-group-register">
									<label style={{...styles}} className={this.state.class}>
										Username
									</label>
									<input type="text" name="username" className="form-controls-register" onChange={this.handleChange} />
								</div>
							</Col>
							<Col sm="12" md="6" lg="6" xl="6">
								<div className="form-group-register">
									<label style={{...styles}} className={this.state.class}>
										Password
									</label>
									<input type="password" name="password" className="form-controls-register" onChange={this.handleChange} />
								</div>
							</Col>
							<Col sm="12" md="6" lg="6" xl="6">
								<div className="form-group-register">
									<label style={{...styles}} className={this.state.class}>
										Retype Password
									</label>
									<input type="password" name="repass" className="form-controls-register" onChange={this.handleChange} />
								</div>
							</Col>
							<Col sm="12" md="6" lg="6" xl="6">
								<div className="form-group-register">
									<span>Pertanyaan Kemanan</span>
									<select className="selecr" name="idPertanyaanKeamanan" onChange={this.handleChange}>
									{
										pertanyaan.length > 0 ? pertanyaan.map(quest => {
											return <option key={quest.id} value={quest.id}>{quest.pertanyaan}</option>
										}) : null
									}
									</select>
								</div>
							</Col>
							<Col sm="12" md="6" lg="6" xl="6">
								<div className="form-group-register">
									<label style={{...styles}} className={this.state.class}>
										Jawaban Keamanan
									</label>
									<input type="text" name="jawabanKeamanan" className="form-controls-register"  onChange={this.handleChange}/>
								</div>
							</Col>
							<Col>
								<div className="form-group-register">
									<input type="submit" className="form-controls-register" onClick={this.handelSubmit} value="DAFTAR"/>
								</div>
							</Col>
						</Row>
					</div>
				</div>
			</div>
			);
	}
}