import React, { Component } from 'react';
import {
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';
import Style from './register.css'

import Register from './component-register'

import MenuNavbar from './../Menu'
import Footer from './../Footer'
export default class register extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
				<Register/>
				<Footer/>
			</div>
			);
	}
}