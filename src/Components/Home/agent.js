import React, { Component } from 'react';
import { Card,
		 CardImg, 
		 CardText, 
		 CardBody, 
		 CardTitle, 
		 CardSubtitle, 
		 Button, 
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';

export default class Kenapa extends Component{
	render(){
		return(
			<div className="lg-pd" style={{textAlign:"center"}}>
				<Row>
					<Col xs="12" sm="12" md="12" lg="3" xl="3">
				      <Card>
				        <CardImg top width="100%" src="https://kerjasama.com/static/media/Menguntungkan.25abfd8b.png" alt="Card image cap" />
				        <CardBody>
				          <CardTitle>Card title</CardTitle>
				          <CardSubtitle>Card subtitle</CardSubtitle>
				          <Button>Button</Button>
				        </CardBody>
				      </Card>
					</Col>
					<Col xs="12" sm="12" md="12" lg="3" xl="3">
				      <Card>
				        <CardImg top width="100%" src="https://kerjasama.com/static/media/Menguntungkan.25abfd8b.png" alt="Card image cap" />
				        <CardBody>
				          <CardTitle>Card title</CardTitle>
				          <CardSubtitle>Card subtitle</CardSubtitle>
				          <Button>Button</Button>
				        </CardBody>
				      </Card>
					</Col>
					<Col xs="12" sm="12" md="12" lg="3" xl="3">
				      <Card>
				        <CardImg top width="100%" src="http://www.basecampbogor.com/wp-content/uploads/agents/big/raenaldi.jpg" alt="Card image cap" />
				        <CardBody>
				          <CardTitle>Card title</CardTitle>
				          <CardSubtitle>Card subtitle</CardSubtitle>
				          <Button>Button</Button>
				        </CardBody>
				      </Card>
					</Col>
					<Col xs="12" sm="12" md="12" lg="3" xl="3">
				      <Card>
				        <CardImg top width="100%" src="https://kerjasama.com/static/media/Menguntungkan.25abfd8b.png" alt="Card image cap" />
				        <CardBody>
				          <CardTitle>Card title</CardTitle>
				          <CardSubtitle>Card subtitle</CardSubtitle>
				          <Button>Button</Button>
				        </CardBody>
				      </Card>
					</Col>
				</Row>
			</div>
			);
	}
}