import React, { Component } from 'react';
import {
		 Container, 
		 Row, 
		 Col
		} from 'reactstrap';

export default class Kenapa extends Component{
	render(){
		return(
			<div className="lg-pd" style={{textAlign:"center"}}>
				<Row>
					<Col xs="12" sm="12" md="12" lg="4" xl="4">
						<div className="kenapa-kita">
							<img src="https://kerjasama.com/static/media/Menguntungkan.25abfd8b.png"/>
							<h3>Lorem</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat eros quis neque porta, et rhoncus ligula aliquet.</p>
						</div>
					</Col>
					<Col xs="12" sm="12" md="12" lg="4" xl="4">
						<div className="kenapa-kita">
							<img src="https://kerjasama.com/static/media/Menguntungkan.25abfd8b.png"/>
							<h3>Lorem</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat eros quis neque porta, et rhoncus ligula aliquet.</p>
						</div>
					</Col>
					<Col xs="12" sm="12" md="12" lg="4" xl="4">
						<div className="kenapa-kita">
							<img src="https://kerjasama.com/static/media/Menguntungkan.25abfd8b.png"/>
							<h3>Lorem</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat eros quis neque porta, et rhoncus ligula aliquet.</p>
						</div>
					</Col>
				</Row>
			</div>
			);
	}
}