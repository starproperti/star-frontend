import React, { Component } from 'react';
import {Link} from "react-router-dom";
import MenuNavbar from './../Menu'
import TopSect from './top-sect'
import KenapaKita from './kenapa'
import Apaitu from './apaitu'
import Content from './../Property/listProperty'
import ContentPropertyLimit from './../Property/listPropertyLimit'
import ContentPortofolio from './../Portofolio/portofolio'
import ContentPortofolioLimit from './../Portofolio/portofolioLimit'
import Agent from './agent'
import Footer from './../Footer'

import style from './home.css'
//import 'whatwg-fetch';

class Home extends Component{	
	render(){
		return(
			<div>
				<MenuNavbar/>
				<TopSect/>
					
				<div style={{background:"#f2f2f2"}}>
					<div className="our-property">
						<h3>Apa itu StarProperti ?</h3>
					</div>
					<Apaitu/>
				</div>
				<div style={{background:"#f2f2f2"}}>
					<div className="our-property">
						<h3>Kenapa StarProperti ?</h3>
						<p></p>
					</div>
					<KenapaKita/>
				</div>

				<div className="our-property">
					<h3>Properti Terbaru</h3>
					<p><b>lihat properti terbaru</b></p>
				</div>
				<div className="lg-pd">
					<ContentPropertyLimit/>
					<div className="our-property">
						<Link to="Property" style={{border:"1px solid rgba(0,0,0,0.1)"}}>Semua Properti</Link>
					</div>
				</div>

				<div className="our-property">
					<h3>Portofolio</h3>
					<p></p>
				</div>
				<div className="lg-pd">
					<ContentPortofolioLimit/>
					<div className="our-property">
						<Link to="Portofolio" style={{border:"1px solid rgba(0,0,0,0.1)"}}>Semua Portofolio</Link>
					</div>
				</div>
				
				{/*<div className="our-property">
					<h3>Agent Kami</h3>
					<p></p>
				</div>
				<Agent/>*/}
				<Footer/>
			</div>
			);
	}
}
export default Home;