import React, { Component } from 'react';
import {Link} from "react-router-dom";
import {Row, Col } from 'reactstrap';

import Style from './Menu.css'

export default class Menu extends Component{
    constructor(props){
        super(props);
        this.state = {
            class:''
        };
    }

    toggleClass(){
        this.setState({class:this.state.class === 'push-side-left-page' ? '' : 'push-side-left-page'})
    }
    closeClass(){
        this.setState({class:this.state.class === '' ? 'push-side-left-page' : '' })
    }
	render(){
		return(
            <div>
                <header>
                    <Row>
                        <Col lg="4" md="6" sm="2">
                            <div className="bt-menu" onClick={this.toggleClass.bind(this)}>
                                <div className="menu-bar-left" id="xBar">
                                    <div className="container-menu-bar-left">
                                        <div className="bar-menu-bar-left"></div>
                                        <div className="bar-menu-bar-left"></div>
                                        <div className="bar-menu-bar-left"></div>
                                    </div>
                                    <div className="menu-item-left-bar">
                                        Menu
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col lg="4" md="6" sm="10">
                            <div className="logo">
                                <Link to="/"><img src="http://starproperti.com/images/biodata/2018-06-09-10-41-04logo-horizontal-new.png"/></Link>
                            </div>
                        </Col>
                        <Col lg="4" md="4" sm="6" id="dp-none">
                            <div className="row np">
                                <div className="col-lg-6 btn-regist np">
                                    <div>
                                        <Link to="/Login">Sign In</Link>
                                    </div>
                                </div>
                                <div className="col-lg-6 btn-regist np">
                                    <div>
                                        <Link to="/Register">Register</Link>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </header>
                <div id="navsidebar" className={this.state.class}>
                    <label className="close" onClick={this.closeClass.bind(this)}>&times;</label>
                        <ul>
                            <li>
                                <Link to="/" style={{marginTop:68}}>Beranda</Link>
                            </li>
                            <li>
                                <Link to="/Property">Property</Link>
                            </li>
                            <li>
                                <Link to="/Portofolio">Portofolio</Link>
                            </li>
                            <li>
                                <Link to="/Agent">Agents</Link>
                            </li>
                            <li>
                                <Link to="/Tentang">Tentang</Link>
                            </li>
                            <li>
                                <Link to="/Contact">Contact</Link>
                            </li>
                            <li className="login-dp">
                                <Link to="/Register">Register</Link>
                            </li>
                            <li className="login-dp">
                                <Link to="#">Sign In</Link>
                            </li>
                        </ul>
                </div>
            </div>
			);
	}
}