import React, { Component } from 'react';
import {Link} from "react-router-dom";

import ContactComponent from './contact-component'
import MenuNavbar from './../Menu'
import Footer from './../Footer'

export default class IndexContact extends Component{
	render(){
		return(
			<div>
				<MenuNavbar/>
					<ContactComponent/>
				<Footer/>
			</div>
			);
	}
}