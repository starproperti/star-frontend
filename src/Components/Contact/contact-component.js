import React, { Component } from 'react';
import {Link} from "react-router-dom";
import {
		 Row, 
		 Col
		} from 'reactstrap'
import './Contact.css'

export default class ContactComponent extends Component{
	render(){
		return(
			<div className="container-top-sect lg-pd">
				<Row className="container-top-sect">
					<Col xs="12" sm="12" md="3" className="np">
						<h1 className="font-up-center" style={{fontSize:35}}>Informasi StarProperti</h1>
					</Col>
					<Col xs="12" sm="12" md="9">
						<div className="box-card">
							<h5>Tentang Kami</h5>
							<ul className="list-contact">
								<li><i className="fa fa-map-marker"></i><span><a href="https://www.google.com/maps/place/de+Prima+Tunggulwulung/@-7.9233291,112.6186174,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7882006892fbd7:0x95460c42824606df!8m2!3d-7.9233291!4d112.6186174">Ruko kav.1 De Prima Tunggulwulung Jl. Akordion Kel. Tunggulwulung Kec. Lowokwaru Kota Malang</a></span></li>
								<li><i className="fa fa-envelope"></i><span>pam.official17@gmail.com</span></li>
								<li><i className="fa fa-phone"></i><span>081252129988</span></li>
							</ul>
						</div>
					</Col>
				</Row>
			</div>
			);
	}
}