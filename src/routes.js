import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from './Container/HomeContainer'
import Property from './Container/PropertyContainer'
import DetailProperty from './Container/DetailPropertyContainer'
import Portofolio from './Container/PortofolioContainer'
import DetailPortofolio from './Container/DetailPortofolioContainer'
import Agent from './Container/AgentContainer'
import Register from './Container/RegisterContainer'
import Login from './Container/LoginContainer'
import Tentang from './Container/TentangContainer'
import Contact from './Container/ContactContainer'
import NotFound from './Components/NotFound'
import ScrollToTop from './Components/Scroll/scrollTop'

class Routes extends Component{
	render(){
		return(
			<Router>
			<ScrollToTop>
				<Switch>
					<Route exact path="/" component={Home}/>
					<Route exact path="/Property" component={Property}/>
					<Route exact path="/Detail-Property/:Property/:id" component={DetailProperty}/>
					<Route exact path="/Portofolio" component={Portofolio}/>
					<Route exact path="/Detail-Portofolio/:Portofolio/:id/:status" component={DetailPortofolio}/>
					<Route exact path="/Agent" component={Agent}/>
					<Route exact path="/Tentang" component={Tentang}/>
					<Route exact path="/Contact" component={Contact}/>
					<Route exact path="/Register" component={Register}/>
					<Route exact path="/Login" component={Login}/>
					<Route exact component={NotFound}/>
				</Switch>
			</ScrollToTop>
			</Router>
			);
	}
}

export default Routes;